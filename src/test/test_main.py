from src.main.python.main import SparkClass, Hotels, Weather, CoordinateUtils

apiKey = '28f63929ef344046957629c90822bfa2'
CoordUtil = CoordinateUtils(apiKey)
path_hotels = 'src/test/resources/hotels/*.csv.gz'
path_weather = 'src/test/resources/weather/'
SparkObj = SparkClass()
Hotels = Hotels(path_hotels, SparkObj, CoordUtil)
Weather = Weather(path_weather, SparkObj, CoordUtil)


def test_add_col_query():
    # add additional column with string for query to Rest Api
    Hotels.add_col_qeury()
    assert (Hotels.df.limit(100).toPandas().iloc[0][-1] == 'exist' and
            Hotels.df.limit(100).toPandas().iloc[35][-1] != 'exist')


def test_add_col_coord():
    # add additional column with merged coordinates latitude and longitude
    Hotels.add_col_coord()
    assert (Hotels.df.limit(100).toPandas().iloc[35][-1] == '38.9608158, -77.423214')


def test_add_latitude():
    # add latitude value for missing in latitude column
    Hotels.add_latitude()
    assert (Hotels.df.limit(100).toPandas().iloc[35][5] == '38.9608158')


def test_add_longitude():
    # add longitude value for missing in latitude column
    Hotels.add_longitude()
    assert (Hotels.df.limit(100).toPandas().iloc[35][6] == ' -77.423214')


def test_drop_columns():
    # drop columns 'Longitude', 'Latitude', 'QueryColumn', 'LatLong'
    Hotels.drop_columns()
    assert (len(Hotels.df.limit(100).toPandas().columns) == 5)


def test_add_geohash_to_weather():
    # calculate and add geohash column
    Weather.add_col_geohash()
    assert (Weather.df.limit(100).toPandas().iloc[35][-1] == 'c8zx' and
            Weather.df.limit(100).toPandas().iloc[0][-1] == 'c8yn')


def test_drop_geohash_dublicates():
    # drop dublicates
    Weather.df = Weather.df.dropDuplicates(['Geohash', 'wthr_date'])
    assert Weather.df.count() == 8899
