from src.main.python.main import CoordinateUtils

apiKey = '28f63929ef344046957629c90822bfa2'
CoordUtil = CoordinateUtils(apiKey)


def test_get_coordinates():
    # get coordinates from Rest Api
    query_str = 'Americana Resort Properties, 135 Main St, Dillon, US'
    coord_from_api = CoordUtil.get_coordinates(query_str)
    assert coord_from_api == '37.944598, -91.773614'


def test_geo_hash():
    # calculate geohash
    assert [CoordUtil.geohash(48.6, -4.39), CoordUtil.geohash(100, 100)] == ['gbsu', 'ypzp']
