from pyspark import SparkConf
from pyspark.sql import SparkSession
from pyspark.sql.functions import when, concat_ws, udf, split, col
from pyspark.sql.types import StringType
import pygeohash as pgh
import requests


class CoordinateUtils:
    """Coordinates and geohash class """
    def __init__(self, apikey):
        # api request variable
        self.apiReq = None
        # geohash variable
        self.encodedGeohash = None
        # apikey for Rest Api access
        self.apiKey = apikey

    def get_coordinates(self, data_for_query):
        # Get Latitude and Longitude from Rest Api
        if data_for_query != "exist":
            # api request
            resp = requests.get('https://api.opencagedata.com/geocode/v1/json?q=' +
                                data_for_query + '&key=' + self.apiKey)
            # convert request to json
            data = resp.json()
            # unite latitude and longitude with delimiter
            self.apiReq = str(data['results'][0]['geometry']['lat']) + ", " + str(data['results'][0]['geometry']['lng'])
        return self.apiReq

    def geohash(self, latitude, longitude):
        # Calculates 4 length geohash from latitude and longitude
        self.encodedGeohash = pgh.encode(float(latitude), float(longitude), 4)
        return self.encodedGeohash


class SparkClass:
    """Class for spark"""

    def __init__(self):
        # SparkSession build
        conf = SparkConf().setAll(
            [('spark.executor.memory', '1g'), ('spark.executor.cores', '1'), ('spark.cores.max', '1'),
             ('spark.driver.memory', '1g')])
        self.spark_unit = SparkSession \
            .builder \
            .appName("SparkBasics") \
            .config(conf=conf).getOrCreate()


class Hotels:
    """ Class for hotels data manipulation """

    def __init__(self, path_to_hotels, spark, coordutil):
        # Read csv to DataFrame
        self.df = spark.spark_unit.read.option("header", "true").csv(path_to_hotels)
        # udf for coordinate calculation
        self.coord_udf = udf(lambda x: coordutil.get_coordinates(x), StringType())
        # udf for geohash calculation
        self.geo_hash_calc = udf(coordutil.geohash, StringType())

    def add_col_qeury(self):
        # add additional column with string for query to Rest Api
        self.df = self.df.withColumn("QueryColumn", when(
            self.df.Latitude.isNull() | self.df.Longitude.isNull() |
            (self.df.Latitude == "NA") | (self.df.Longitude == "NA"),
            concat_ws(", ", "Name", "Address", 'City', 'Country')).otherwise("exist"))

    def add_col_coord(self):
        # add additional column with merged coordinates latitude and longitude
        self.df = self.df.withColumn("LatLong", when((self.df.QueryColumn != 'exist'),
                                                     self.coord_udf(col("QueryColumn"))).otherwise(""))

    def add_latitude(self):
        # add latitude value for missing in latitude column
        self.df = self.df.withColumn("Latitude", when((self.df.Latitude.isNull()) | (self.df.Latitude == 'NA'),
                                                      split(self.df['LatLong'], ',').getItem(0)).otherwise(
            self.df.Latitude))

    def add_longitude(self):
        # add longitude value for missing in longitude column
        self.df = self.df.withColumn("Longitude", when((self.df.Longitude.isNull()) | (self.df.Longitude == 'NA'),
                                                       split(self.df['LatLong'], ',').getItem(1)).otherwise(
            self.df.Longitude))

    def add_col_geohash(self):
        # calculate and add geohash column
        self.df = self.df.withColumn("Geohash", self.geo_hash_calc('Latitude', 'Longitude'))

    def drop_columns(self):
        # drop columns 'Longitude', 'Latitude', 'QueryColumn', 'LatLong'
        columns_to_drop = ['Longitude', 'Latitude', 'QueryColumn', 'LatLong']
        self.df = self.df.drop(*columns_to_drop)

    def processing(self):
        # Hotels data manipulation
        self.add_col_qeury()
        self.add_col_coord()
        self.add_latitude()
        self.add_longitude()
        self.add_col_geohash()
        self.drop_columns()


class Weather:
    """Class for weather data manipulation"""

    def __init__(self, path_to_weather, spark, coordutil):
        # Read parquet to DataFrame
        self.df = spark.spark_unit.read.parquet(path_to_weather)
        # udf for geohash calculation
        self.geo_hash_calc = udf(coordutil.geohash, StringType())

    def add_col_geohash(self):
        # calculate and add geohash column
        self.df = self.df.withColumn("Geohash", self.geo_hash_calc('lat', 'lng'))

    def drop_geohash_dublicates(self):
        # drop dublicates
        self.df = self.df.dropDuplicates(['Geohash', 'wthr_date'])

    def left_join_hotels(self, hotel_df):
        # left join with hotels DataFrame
        self.df = self.df.join(hotel_df, self.df.Geohash == hotel_df.Geohash, how='left').drop('Geohash')

    def write_parquet(self, path):
        # write parquet file
        self.df.write.partitionBy('year', 'month', 'day').mode("overwrite").parquet(path)

    def processing(self, hotels_df, outputpath):
        # Weather data manipulation
        self.add_col_geohash()
        self.drop_geohash_dublicates()
        self.left_join_hotels(hotels_df)
        self.write_parquet(outputpath)


if __name__ == '__main__':
    path_hotels = 'resources/hotels/*.gz'
    path_weather = 'resources/weather/'
    output_path = '/resources/output/'
    apiKey = '28f63929ef344046957629c90822bfa2'
    CoordUtil = CoordinateUtils(apiKey)
    SparkObj = SparkClass()
    Hotels = Hotels(path_hotels, SparkObj, CoordUtil)
    Hotels.processing()
    Weather = Weather(path_weather, SparkObj, CoordUtil)
    Weather.processing(Hotels.df, output_path)
    SparkObj.spark_unit.stop()
