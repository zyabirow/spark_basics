* Setup needed requirements into your env `pip install -r requirements.txt`
* Build docker image
```
docker build -t <yourpysparkdocker>
```
* Push docker image to Azure Container Registry
```
docker tag <yourpysparkdocker> <yourazureregaccount>.azurecr.io/<yourpysparkdocker>
docker push <yourazureregaccount>.azurecr.io/<yourpysparkdocker>
```

* Deploy infrastructure with terraform

```
terraform init
terraform plan -out terraform.plan
terraform apply terraform.plan
```
* Final table view
![finaltable](images/final_table.jpg)
****
